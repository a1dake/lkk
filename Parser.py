from datetime import datetime, timedelta
from bs4 import BeautifulSoup
import requests
import cfscrape
from lxml import html
import sys
import os

currentdir = os.path.dirname(os.path.realpath(__file__))
sys.path.append("/home/manage_report")

from Send_report.mywrapper import magicDB


class Parser:
    def __init__(self, parser_name: str):
        self.session = cfscrape.create_scraper(sess=requests.Session())
        self.result_data: dict = {'name': parser_name,
                                  'data': []}

    @magicDB
    def run(self):
        content: list = self.get_content()
        self.result_data['data'] = content
        print(content)
        return self.result_data

    def get_urls(self):
        date_from = datetime.strftime((datetime.now() - timedelta(days=1)), '%d.%m.%Y %H:00')
        date_to = datetime.strftime((datetime.now()), '%d.%m.%Y %H:00')
        print('from: {}, to: {}'.format(date_from, date_to))

        ads = []
        url = "https://lkk.irkutskoil.ru/active-tenders/list?top=false"
        response = self.session.get(url, timeout=10)
        soup = BeautifulSoup(response.content, 'html.parser')
        for link in soup.find_all('div', class_='tender row'):
            tender_url = 'https://lkk.irkutskoil.ru/' + link.select_one('div', class_='tender-title col-12 col-md-6').find('a').get('href')
            tender_date = ''.join(link.select('div', class_='tender-details col-12 col-md-4 mt-4 mb-4')[3].select('div')[2].get_text().split(' (GMT+8)')[0])

            date = datetime.strptime(tender_date, '%d.%m.%Y %H:%M') - timedelta(hours=5)
            from_date = datetime.strptime(date_from, '%d.%m.%Y %H:%M')
            to_date = datetime.strptime(date_to, '%d.%m.%Y %H:%M')
            if from_date < date < to_date:
                ads.append(tender_url)
        return ads

    def get_content(self):
        ads = self.get_urls()
        contents = []
        for link in ads:
            item_data = {
                'type': 2,
                'title': '',
                'purchaseNumber': '',
                'fz': 'Коммерческие',
                'purchaseType': '',
                'url': '',
                'attachments': [],
                'procedureInfo': {
                    'endDate': ''
                },

                'customer': {
                    'fullName': 'ООО "ИРКУТСКАЯ НЕФТЯНАЯ КОМПАНИЯ"',
                    'factAddress': '664007, Иркутская область, город Иркутск, Большой Литейный пр-кт, д. 4',
                    'inn': 3808066311,
                    'kpp': 3808066311,
                }
            }

            try:
                response = self.session.get(link, timeout=30)
                tree = html.document_fromstring(response.content)

                item_data['title'] = str(self.get_title(tree))
                item_data['url'] = str(link)
                item_data['purchaseNumber'] = str(self.get_number(tree))

                item_data['procedureInfo']['endDate'] = self.get_end_date(tree)

                item_data['attachments'] = self.get_attach(tree)

                contents.append(item_data)

            except Exception as e:
                print(f"{e} - Страница с ошибкой ", link)
        return contents

    def get_title(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Наименование процедуры")]/parent::*/./*[2]/text()')).strip()
        except:
            data = ''
        return data

    def get_number(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"№ тендера")]/parent::*/./*[2]/text()')).strip()
        except:
            data = ''
        return data

    def get_end_date(self, tree):
        try:
            data = ''.join(tree.xpath('//*[contains(text(),"Окончание приёма заявок")]/parent::*/./*[2]/text()')).strip()[0:16]
            formatted_date = self.formate_date(data)
        except:
            formatted_date = None
        return formatted_date

    def get_attach(self, tree):
        try:
            data = []
            doc_div = tree.xpath('//div[@class="collapse-body "]/div[@class="tender-sections-wrap mb-3"]/div[@class="tender-detail-section"]/div[@class="tender-detail-section-row d-md-flex"]')
            for doc in doc_div:
                docs = {'docDescription': ''.join(doc.xpath('div[2]/text()')).strip(),
                        'url': 'https://lkk.irkutskoil.ru/' + ''.join(doc.xpath('div[1]/a/@href'))}

                data.append(docs)
        except:
            data = []
        return data

    def formate_date(self, old_date):
        date_object = datetime.strptime(old_date, '%d.%m.%Y %H:%M')
        formatted_date = date_object.strftime('%H.%M.%S %d.%m.%Y')
        return formatted_date
